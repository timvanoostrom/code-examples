/*

const database = require('database');

const options = {
    json: true, // parses/stringifies all passed content to/from json
    queue: true // use the internal queue by default
}

// create collection
const db = database.create('databaseName', options);
const collection = db.collection('collectionName');

database.onReady(() => {
    // database operations can start, possible migration has finished
});
collection.onReady(() => {
    // database operations can start, possible migration has finished
});

// reads file content
collection.read();
- or -
db.read('collectionName');

// writes content
collection.write(content);
- or -
db.write('collectionName', content);

// removes file
collection.remove(); // removes file
- or -
db.remove('collectionName');

// truncates file
collection.truncate();
- or -
db.truncate('collectionName');

// Atomic crud function, a series of read/write/set/get/truncate events that act as 1 operation before
// next queued operation will start. This will prevent undesired results when reading.then.writing simultaneously

const atomicRW = () => collection.atomic((atomicAPI) => {
    return atomicAPI.read().then((content) => {
        // modify content here
        return write(content);
    });
});

// JSON database using set/get/unset methods

// set specific path in database
collection.set('foo.bar', 'value');
- or -
db.set('collectionName', 'foo.bar', 'value');

// get specific path in database
collection.get('foo.bar');
- or -
db.get('collectionName', 'foo.bar');

// unset specific path in database
collection.unset('foo.bar');
- or -
db.unset('collectionName', 'foo.bar');

 */

const fs = require('fs');
const path = require('path');

const _set = require('lodash.set');
const _get = require('lodash.get');
const _has = require('lodash.has');
const _unset = require('lodash.unset');

const {
    deleteFolderContentRecursive,
    createFolderIfNotExists,
    fileExists,
    readFile,
    writeFile,
    deleteFile,
    writeJson,
    readJson,
    readDir,
    truncate
} = require('./fs-helpers');
const log = require('./logger').of(module);

const DB_FOLDER = path.join(process.cwd(), 'data');
const DEFAULT_COLLECTION_NAME = 'main';
const JSON_FILE_EXT = '.json';
const DB_VERSION_FILE = '.version.json';
const RAW_FILE_EXT = '';
const INITIAL_CONTENT_JSON = null; // Will be stringified when written.
const INITIAL_CONTENT = ''; // USE Empty string for raw files.

// collection API methods for general crud operations
const collectionMethods = ['getFilePath', 'read', 'write', 'set', 'get', 'unset', 'has', 'remove', 'truncate', 'onReady', 'queue', 'atomic'];

const IGNORED_FILES = [DB_VERSION_FILE];

const defaultOptions = {
    json: false,
    queue: true
};

function initialize() {
    return createFolderIfNotExists(DB_FOLDER);
}

function writeCurrentVersion(dbFolder, version) {
    return writeJson(path.join(dbFolder, DB_VERSION_FILE), { version });
}

function getCurrentVersion(dbFolder) {
    const returnWithVersion = (version) => {
        return _get(version, 'version', undefined);
    };
    return readJson(path.join(dbFolder, DB_VERSION_FILE)).then(returnWithVersion, returnWithVersion);
}

function upgrade(db, newVersion, onVersionChange) {
    // A migration function is available, perform the migration before allowing
    // requests to the database.
    const dbFolder = db.dbFolder;
    if (onVersionChange) {
        // Pass the DB instance + the cleanup function to the
        // versionChange callback so they can be used to migrate
        // the existing data.
        return Promise.all([onVersionChange(
            db,
            deleteFolderContentRecursive.bind(null, this.dbFolder)
        )]).then(() => {
            return writeCurrentVersion(dbFolder, newVersion);
        }).catch((error) => {
            log.error('An error in the database migration of', this.name, 'to', newVersion, 'occured with error:\n\n',
                error, '\n\nthe current database will just be deleted.');
            // Just resume with normal clean-slate operation without migrating data.
            return deleteFolderContentRecursive(dbFolder).then(() => {
                return writeCurrentVersion(dbFolder, newVersion);
            });
        });
    }
    // There is a new version but no migration needed, just remove the database contents.
    return deleteFolderContentRecursive(dbFolder).then(() => {
        return writeCurrentVersion(dbFolder, newVersion);
    });
}

function noQueue(options = {}) {
    return Object.assign(options, { queue: false });
}

class DatabaseBase {
    constructor(name, options = {}) {
        this.name = name.replace(/\//g, '__');
        this.dbFolder = path.join(DB_FOLDER, this.name);
        this.options = Object.assign({}, defaultOptions, options);
        this.store = {};
        this.fileSystemQueue = {};
    }

    initialize() {
        if (!this.initializationPromise) {
            const version = this.options.version || 0;
            this.initializationPromise = getCurrentVersion(this.dbFolder).then((currentVersion) => {
                if (typeof currentVersion !== 'undefined') {
                    if (version > currentVersion) {
                        this.upgradePromise = upgrade(this, version, this.options.onVersionChange);
                    }
                }
                // The very first time the database is accessed.
                return createFolderIfNotExists(this.dbFolder).then(() => {
                    return writeCurrentVersion(this.dbFolder, version);
                });
            });
        }
        return this;
    }

    collection(name) {
        const api = {
            onReady: this.onReady.bind(this)
        };
        for (const method of collectionMethods) {
            if (typeof this[method] === 'function') {
                api[method] = this[method].bind(this, name);
            }
        }
        return api;
    }

    onReady() {
        return this.initializationPromise.then(() => {
            return Promise.all([
                this.upgradePromise
            ]);
        });
    }

    getFilePath(collection) {
        const collectionName = (collection && collection.replace(/\//g, '__')) || `${this.name}-${DEFAULT_COLLECTION_NAME}`;
        const name = path.join(this.dbFolder, collectionName);
        const filePath = this.options.json ? name + JSON_FILE_EXT : name + RAW_FILE_EXT;
        return filePath;
    }

    isJSON(options = {}) {
        return typeof options.json !== 'undefined' ? options.json : this.options.json;
    }

    shouldQueue(options = {}) {
        return typeof options.queue !== 'undefined' ? options.queue : this.options.queue;
    }

    getCollections() {
        return readDir(this.dbFolder).then((files) => {
            const filesList = files.filter(file => !IGNORED_FILES.includes(file));
            return this.options.json ? filesList.map(file => file.substring(0, file.lastIndexOf(JSON_FILE_EXT))) : filesList;
        });
    }

    queue(collection, callback) {
        if (!this.fileSystemQueue[collection]) {
            this.fileSystemQueue[collection] = {
                promise: this.initializationPromise.then(() => undefined),
                id: 0
            };
        }
        // The queue will not be interrupted by individual database
        // operation rejections. This could lead to unexpected results when relying on happy path where every operation
        // succeeds. The individual operations do return a rejected promise so you can still act upon
        // individual operations in the queue.
        const queuedPromise = this.fileSystemQueue[collection].promise.then(callback, callback);
        const id = this.fileSystemQueue[collection].id + 1;
        this.fileSystemQueue[collection].id = id;

        this.fileSystemQueue[collection].promise = queuedPromise.then((result) => {
            if (id === this.fileSystemQueue[collection].id) {
                delete this.fileSystemQueue[collection];
            }
            return result;
        }, (result) => {
            if (id === this.fileSystemQueue[collection].id) {
                delete this.fileSystemQueue[collection];
            }
            return result;
        });

        return queuedPromise;
    }

    atomic(collection, callback) {
        const atomicApi = {
            read: (options) => {
                return this.read(collection, noQueue(options));
            },
            write: (content, options) => {
                return this.write(collection, content, noQueue(options));
            },
            truncate: (options) => {
                return this.truncate(collection, noQueue(options));
            }
        };
        if (this.isJSON()) {
            Object.assign(atomicApi, {
                set: (path, value, options) => {
                    return this.set(collection, path, value, noQueue(options));
                },
                get: (path, options) => {
                    return this.get(collection, path, noQueue(options));
                },
                unset: (path, options) => {
                    return this.unset(collection, path, noQueue(options));
                }
            });
        }
        return this.queue(collection, callback.bind(this, atomicApi));
    }

    truncate(collection, options = {}) {
        const filePath = this.getFilePath(collection);
        return this.shouldQueue(options) ? this.queue(collection, () => truncate(filePath)) : truncate(filePath);
    }

    remove(collection, options = {}) {
        const filePath = this.getFilePath(collection);
        const remove = () => {
            return fileExists(filePath).then((exists) => {
                if (exists) {
                    return deleteFile(filePath);
                }
                return Promise.resolve(`remove failed, ${filePath} does not exist`);
            });
        };

        return this.shouldQueue(options) ? this.queue(collection, remove) : remove();
    }
}

class Database extends DatabaseBase {

    read(collection, options = {}) {
        const read = () => {
            const filePath = this.getFilePath(collection);
            const isJSON = this.isJSON(options);

            return fileExists(filePath).then((exists) => {
                if (exists) {
                    return readFile(filePath, isJSON);
                }
                return isJSON ? INITIAL_CONTENT_JSON : INITIAL_CONTENT;
            });
        };

        return this.shouldQueue(options) ? this.queue(collection, read) : read();
    }

    write(collection, content, options = {}) {
        const write = () => {
            const filePath = this.getFilePath(collection);
            const isJSON = this.isJSON(options);

            return writeFile(filePath, content, isJSON);
        };

        return this.shouldQueue(options) ? this.queue(collection, write) : write();
    }
}

class DatabaseJSON extends Database {

    // Works somewhat like _.get() but with the posibility to get something from
    // a specific collection.
    get(collection, path, options) {
        return this.read(collection, options).then((collectionData) => {
            return _get(collectionData, path);
        });
    }

    // Works somewhat like _.set() but with the posibility to set something in
    // a specific collection.
    set(collection, path, value, options = {}) {
        const setValue = () => {
            return this.read(collection, Object.assign(options, { queue: false })).then((collectionData) => {
                collectionData = (collectionData !== INITIAL_CONTENT_JSON && typeof collectionData === 'object') ? collectionData : {};
                _set(collectionData, path, value);
                return this.write(collection, collectionData, { queue: false });
            });
        };

        return (this.shouldQueue(options) ? this.queue(collection, setValue) : setValue()).then(() => value);
    }

    // Works somewhat like _.has() but with the posibility to check something in
    // a specific collection.
    has(collection, path, options) {
        return this.read(collection, options).then((collectionData) => {
            return _has(collectionData, path);
        });
    }

    // Works somewhat like _.unset() but with the posibility to unset something in
    // a specific collection.
    unset(collection, path, options = {}) {
        const unset = () => {
            return this.read(collection, Object.assign(options, { queue: false })).then((collectionData) => {
                if (_has(collectionData, path)) {
                    _unset(collectionData, path);
                    return this.write(collection, collectionData, Object.assign(options, { queue: false }));
                }
                return collectionData;
            });
        };

        return this.shouldQueue(options) ? this.queue(collection, unset) : unset();
    }
}

const sizeBytes = {
    KB: 1024,
    MB: 1048576
};

class DatabaseStream extends DatabaseBase {

    constructor(...args) {
        super(...args);
        this.writableStreams = new Map();
        this.readableStreams = new Map();
        this.initialSizeInBytes = new Map();
    }

    startWrite(collectionName) {
        const filePath = this.getFilePath(collectionName);
        if (!this.writableStreams.has(collectionName)) {
            // All chunks will be appended to the current file contents
            this.writableStreams.set(collectionName, fs.createWriteStream(filePath, { flags: 'a' }));
        }
        return new Promise((resolve) => {
            this.writableStreams.get(collectionName).once('open', () => {
                fs.stat(filePath, (err, stat) => {
                    const initialSize = (stat && stat.size) || 0;
                    this.initialSizeInBytes.set(collectionName, initialSize);
                    resolve();
                });
            });
        });
    }

    writeChunk(collectionName, content) {
        return new Promise((resolve, reject) => {
            this.writableStreams.get(collectionName).write(content, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(this.streamSizeSync(collectionName));
                }
            });
        });
    }

    endWrite(collectionName) {
        return new Promise((resolve) => {
            if (this.writableStreams.has(collectionName)) {
                const collection = this.writableStreams.get(collectionName);
                collection.once('close', () => {
                    this.writableStreams.delete(collectionName);
                    resolve();
                });
                collection.end();
            } else {
                resolve();
            }
        });
    }

    startRead(collectionName) {
        const filePath = this.getFilePath(collectionName);
        if (!this.readableStreams.has(collectionName)) {
            this.readableStreams.set(collectionName, fs.createReadStream(filePath));
        }
        return Promise.resolve(this.readableStreams.get(collectionName));
    }

    streamSizeSync(collectionName) {
        return this.initialSizeInBytes.get(collectionName) + this.writableStreams.get(collectionName).bytesWritten;
    }

    endRead(collectionName) {
        return new Promise((resolve) => {
            if (this.readableStreams.has(collectionName)) {
                const collection = this.readableStreams.get(collectionName);
                collection.once('close', () => {
                    this.readableStreams.delete(collectionName);
                    resolve();
                });
                collection.destroy();
            } else {
                resolve();
            }
        });
    }
}

function create(name, options = {}) {
    if (options.json) {
        return new DatabaseJSON(name, options).initialize();
    }
    return new Database(name, options).initialize();
}

function createStreamed(name, options = {}) {
    return new DatabaseStream(name, options).initialize();
}

module.exports = {
    initialize,
    create,
    createStreamed,
    sizeBytes,
    getCurrentVersion,
    readFile
};
