/* eslint-disable */
const assert = require('assert');
const sinon = require('sinon');

const path = require('path');
const proxyquire = require('proxyquire').noCallThru();

const databaseContent = {};

const fsStubs = {
    stat(fileName, callback) {
        callback(null);
    },
    readdir(fileName, callback) {
        callback(null);
    },
    lstat(fileName, callback) {
        callback(null, {
            isDirectory: () => true
        });
    },
    readFile(fileName, encoding, callback) {
        if (typeof databaseContent[fileName] === 'undefined') {
            databaseContent[fileName] = 'null';
        }
        if (fileName === 'zero-length') {
            callback(null, '');
        } else if (fileName === path.join('test-version-file', '.version.json')) {
            callback(null, '{"version":1}');
        } else {
            callback(null, databaseContent[fileName]);
        }
    },
    writeFile(fileName, content, options, callback) {
        databaseContent[fileName] = content;
        callback(null);
    },
    open(fileName, mode, callback) {
        callback(null, fileName);
    },
    close(fileName, callback) {
        callback(null, fileName);
    },
    fsync(fileName, callback) {
        callback(null);
    },
    unlink(fileName, callback) {
        callback(null);
    },
    // stat(fileName, callback) {
    //     callback(new Error('folder does not exist'));
    // },
    mkdir(fileName, callback) {
        callback(null, true);
    },
    truncate(fileName, length, callback) {
        delete databaseContent[fileName];
        callback(null);
    }
};

const fsHelpersStubbed = proxyquire('../../app/common/fs-helpers', {
    fs: fsStubs,
    '@global': true
});

const database = proxyquire('../../app/common/database', {
    './fs-helpers': fsHelpersStubbed
});

const name = 'test';
const initalDataJSON = { foo: { bar: { hello: 'world' } } };
const testCollectionName = '~~awesome_new213123,data';
const testPath = 'foo.bar.hello';
const newPathValue = 'New York';
const initalDataRAW = JSON.stringify({ foo: { bar: { hello: 'world' } } });

// const dbFolder = `${__dirname}/../app/db/${name}`;
// const fileNameJSON = `${dbFolder}/${name}-main.json`;
// const collectionFileNameJSON = `${dbFolder}/${testCollectionName}.json`;
// const fileNameRAW = `${dbFolder}/${name}-main`;
// const collectionFileNameRAW = `${dbFolder}/${testCollectionName}`;

describe('Simple data storage solution', function() {
    describe('JSON collection api', function() {
        let db;

        before(function() {
            db = database.create(name, { json: true, version: 1 }).collection('main');
            return db.write(initalDataJSON);
        });

        it('should read the complete default database file', function() {
            return db.read().then((data) => {
                assert.deepEqual(data, initalDataJSON);
            });
        });

        it('should get a path value', function() {
            return db.get(testPath).then((value) => {
                assert.equal(value, initalDataJSON.foo.bar.hello);
            });
        });

        it('should set a new path value', function() {
            return db.set(testPath, newPathValue).then((value) => {
                assert.equal(value, newPathValue);
            });
        });

        it('should get the new path value', function() {
            return db.get(testPath).then((value) => {
                assert.equal(value, newPathValue);
            });
        });

        it('should replace all the data in the database', function() {
            return db.write(initalDataJSON);
        });

        it('should read the new data database', function() {
            return db.read().then((data) => {
                assert.deepEqual(data, initalDataJSON);
            });
        });

        it('should remove the default database file', function() {
            return db.remove();
        });

        after(() => {
            db = null;
        })
    });
    describe('JSON database api', function() {

        let db;

        before(function() {
            db = database.create(name, { json: true, version: 1 });
            return db.onReady();
        });

        it('should create a new collection in the database', function() {
            return db.write(testCollectionName, initalDataJSON).then((data) => {
                assert.deepEqual(initalDataJSON, data);
            });
        });

        it('should set a new path value in the collection', function() {
            return db.set(testCollectionName, testPath, newPathValue).then((value) => {
                assert.equal(value, newPathValue);
            });
        });

        it('should get the new path value from the collection', function() {
            return db.get(testCollectionName, testPath).then((value) => {
                assert.equal(value, newPathValue);
            });
        });

        it('should replace all the data in the collection', function() {
            return db.write(testCollectionName, initalDataJSON);
        });

        it('should read the new data in the collection', function() {
            return db.read(testCollectionName).then((data) => {
                assert.deepEqual(data, initalDataJSON);
            });
        });

        it('should remove the collection file', function() {
            return db.remove(testCollectionName);
        });

        after(function() {
            db = null;
        });
    });

    describe('Database migration', function() {
        let db1;
        let db2;

        before(() => {
            db1 = database.create('migration-test', { json: true, version: 1 }).collection('main');
            return db1.onReady();
        });

        it('should perform migration to new version', function() {
            function onVersionChange() {
                return Promise.resolve();
            }
            const versionChangeSpy = sinon.spy(onVersionChange);
            db2 = database.create('migration-test', { json: true, version: 2, onVersionChange: versionChangeSpy }).collection('main');
            return db2.onReady().then(() => {
                assert.equal(true, versionChangeSpy.calledOnce);
            });
        });

        after(function() {
            db1 = null;
            db2 = null;
        });
    });

    describe('RAW database interactions collection', function() {
        let db;

        before(function() {
            db = database.create(name, { version: 1 }).collection('main');
            return db.onReady();
        });

        it('should write to the database', function() {
            return db.write(initalDataRAW);
        });

        it('should read the complete database', function() {
            return db.read().then((data) => {
                assert.equal(data, initalDataRAW);
            });
        });

        it('should remove the default database file', function() {
            return db.remove();
        });

        after(function() {
            db = null;
        });
    });

    describe('RAW database interactions', function() {
        let db;

        before(function() {
            db = database.create(name, { version: 1 });
            return db.onReady();
        });
        it('should create a new collection in the database', function() {
            return db.write(testCollectionName, initalDataRAW).then((data) => {
                assert.equal(data, initalDataRAW);
            });
        });

        it('should remove the collection file', function() {
            return db.remove(testCollectionName);
        });

        after(function() {
            db = null;
        });
    });

    describe('fileSystem queue', () => {
        let db;

        before(() => {
            db = database.create(name, { json: true, version: 1 });
            return db.onReady();
        });

        it('should queue consecutive calls', () => {
            let c = 0;
            function promisedFunction() {
                const c1 = c;
                c++;
                return new Promise(resolve => {
                    // resolve the promise after a random timeout period
                    // to test the asynchronous behaviour of the queue;
                    const to = Math.random() * 10;
                    setTimeout(() => {
                        resolve(c1);
                    }, to);
                });
            }

            const promises = [];
            const len = 10;
            let i = 0;

            for (i; i < len; i++) {
                promises.push(
                    db.queue('test', promisedFunction)
                );
            }

            assert(db.fileSystemQueue.test.promise instanceof Promise);
            assert.equal(db.fileSystemQueue.test.id, len);

            return Promise.all(promises).then((res) => {
                // queue is deleted
                assert(!db.fileSystemQueue.test);
                assert.deepEqual(res, Array.from(Array(len).keys()))
            });
        });
    });

    describe('atomic queue function', () => {

        let db;

        before(() => {
            db = database.create(name, { json: true, version: 1 }).collection('atomic-test');
            return db.onReady();
        });

        it('should create atomic queue operation', () => {
            function atomicTruncateWrite(value) {
                return db.atomic((db) => {
                    return db.truncate().then(() => db.write(value));
                });
            };

            function atomicUpdate(aValue, wait) {
                return db.atomic((db) => {
                    return db.read().then((content) => {
                        content = content || [];
                        content.push(aValue);
                        return new Promise((resolve) => {
                            db.write(content).then((rs) => {
                                // simulate time needed to manipulate data
                                setTimeout(() => {
                                    resolve(rs);
                                }, wait);
                            });
                        });
                    });
                }).catch(error => console.log(error));
            }

            return Promise.all([
                db.write([]),
                atomicUpdate('foo 1', 100),
                atomicUpdate('foo 2', 50),
                db.write([]),
                atomicUpdate('bar', 10),
                atomicUpdate('foo 2', 90),
                db.read(),
                db.truncate(),
                atomicUpdate('foo 3', 0),
                atomicTruncateWrite('yeah!'),
                db.read()
            ]).then((result) => {
                assert.deepEqual(result[0], []);
                assert.deepEqual(result[1], ['foo 1']);
                assert.deepEqual(result[2], ['foo 1', 'foo 2']);
                assert.deepEqual(result[3], []);
                assert.deepEqual(result[4], ['bar']);
                assert.deepEqual(result[5], ['bar', 'foo 2']);
                assert.deepEqual(result[6], ['bar', 'foo 2']);
                assert.equal(result[7], undefined);
                assert.deepEqual(result[8], ['foo 3']);
                assert.equal(result[9], 'yeah!');
            });
        });

        after(function() {
            db = null;
        });
    });


    describe('FS wrappers', () => {
        it('should be able to read version file', () =>{
            // See database.stubbed for retrieved output
            return database.getCurrentVersion('test-version-file/').then((version) => {
                assert.equal(version, 1);
            });
        });
        it('should be able to read a 0 content length file', () =>{
            return database.readFile('zero-length', true);
        });
    });
});
