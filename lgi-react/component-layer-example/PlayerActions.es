import log from 'log';
import stateStore from 'store/state_store';

// Css
import './toast.scss';

// Service
import * as PlayoutService from 'domains/playout/service';

// Selectors
import {
    isTrailerPlayout,
    isPlayoutActive,
    playoutAsset
} from 'domains/playout/vod/selectors';
import { playoutSourceType } from 'domains/playout/selectors';

// Actions
import RouterActions from 'actions/router_actions';
import { jump, debouncedJumpToEnd } from '../actions';
import { updateBookmark } from 'actions/dynamic_asset_data_actions';
import { showUiOrDetailsPage, enterTrickPlayer, leaveTrickPlayer, play, resetTrickPlayer } from '../actions';
import { tuneLastChannel } from 'actions/playout/channel';

// Helpers
import { getFullScreenLocation } from 'domains/router/Helpers';

// Constants
import { PlayoutSourceTypes } from 'domains/playout/constants';

let cleanPlayoutStatusObserver = null;

function updateBookmarkFromCurrentPlayout() {
    const appState = stateStore.getState();
    if (isPlayoutActive(appState) && !isTrailerPlayout(appState)) {
        const currentPlayoutAsset = playoutAsset(appState);
        const crid = currentPlayoutAsset.get('crid');
        return PlayoutService.getPlayerPosition().then(position => {
            if (crid && position) {
                updateBookmark(crid, position);
            }
        });
    }
    return Promise.resolve();
}

export function goToMidAssetDetailsPage() {
    const appState = stateStore.getState();
    const currentAsset = playoutAsset(appState);
    const crid = currentAsset.get('crid');

    if (crid) {
        const seriesType = currentAsset.get('seriesType');
        // store the current position as bookmark in the details page data state.
        updateBookmarkFromCurrentPlayout().then(() => {
            if (seriesType) {
                RouterActions.goToEpisodeDetailsPage(crid);
            } else {
                RouterActions.goToVodDetailsPage(crid);
            }
        }).catch(log.error);
    }
}

export function showUiOrMidAssetDetailsPage() {
    showUiOrDetailsPage(goToMidAssetDetailsPage);
}

export function jumpForward(keyEvent) {
    if (keyEvent.keyState !== 'repeat') {
        const appState = stateStore.getState();
        const currentAsset = playoutAsset(appState);
        jump('right', currentAsset.get('durationInSeconds'));
        debouncedJumpToEnd(currentAsset.get('durationInSeconds'));
    }
}

function watchPlayoutStatusChange() {
    cleanPlayoutStatusObserver = PlayoutService.createPlayoutStatusObserver((playoutStatus) => {
        switch (playoutStatus.name) {
            case 'BeginningOfContent':
                play();
                break;
        }
    });
}

function stopWatchingPlayoutStatusChange() {
    if (!cleanPlayoutStatusObserver) {
        return;
    }

    cleanPlayoutStatusObserver();
    cleanPlayoutStatusObserver = null;
}

export function stop() {
    RouterActions.goBack();
    tuneLastChannel();
}

// Route callback
export function onLeaveVodPlayer() {
    stopWatchingPlayoutStatusChange();
    return leaveTrickPlayer();
}

export function setupVodPlayer() {
    return enterTrickPlayer().then(() => {
        watchPlayoutStatusChange();
    }).catch(log.error);
}

// Route callback
export function onEnterVodPlayer(nextState, replaceState) {
    const currentPlayoutSourceType = playoutSourceType(stateStore.getState());
    if (currentPlayoutSourceType !== PlayoutSourceTypes.VOD) {
        return replaceState(null, getFullScreenLocation(currentPlayoutSourceType));
    }
    setupVodPlayer();
}

// Simulated route bootstrap
export function onRestartVodPlayer() {
    // Reset the state
    resetTrickPlayer();
    // Stop listening for changes
    onLeaveVodPlayer();
    // Start change listeners
    return setupVodPlayer();
}
