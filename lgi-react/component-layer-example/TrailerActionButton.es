import React from 'react';
import Immutable from 'immutable';
import './trailerActionButton.scss';

import { TYPE_TRANSACTION, TYPE_SUBSCRIPTION } from '../../../constants/vod_traxis_constants';
import { Translater } from 'lang';
import { getRentActionLabel } from '../../../helpers/vod_formatter_helpers';

const WATCH_LABEL = 'DIC_ACTIONS_WATCH';
const SUBSCRIBE_LABEL = 'DIC_ACTIONS_SUBSCRIBE';
const ACTION_BUTTON_LABEL = 'DIC_GENERIC_OK';

function VodPlayoutTrailerButton(props) {

    const { asset } = props;
    let indicationLabel = WATCH_LABEL;

    // TODO move this logic in the selector
    if (!asset.get('isEntitled')) {
        if (asset.get('productType') === TYPE_TRANSACTION && asset.get('offers') && asset.get('offers').size) {
            indicationLabel = getRentActionLabel(asset.get('offers').toJS());
        } else if (asset.get('productType') === TYPE_SUBSCRIPTION) {
            indicationLabel = SUBSCRIBE_LABEL;
        }
    }

    return (
        <div className="trailer-action-field">
            <span className="indication"><Translater content={indicationLabel} /></span>
            <div className="action-button"><Translater content={ACTION_BUTTON_LABEL} /></div>
        </div>
    );
}

VodPlayoutTrailerButton.propTypes = {
    asset: React.PropTypes.instanceOf(Immutable.Map).isRequired
};

VodPlayoutTrailerButton.contextTypes = {
    currentLocale: React.PropTypes.string.isRequired
};

export default VodPlayoutTrailerButton;
