import Immutable from 'immutable';
import React from 'react';
import ContainerComponent from 'horizon4-shared/components/container_component';
import { connect } from 'react-redux';
import KeyMapper from '../../../key_maps/key_mapper';
import TrailerActionButton from './TrailerActionButton';

// Selectors
import {
    playoutAsset,
    isTrailerPlayout
} from 'domains/playout/vod/selectors';
import { currentModalType } from '../../../selectors/index';
import { playout } from 'domains/playout/selectors';

// Actions
import { playOrPurchaseAsset } from './TrailerActions';
import { stop } from './PlayerActions';

class VodTrailerPlayer extends ContainerComponent {

    constructor(props) {
        super(props);

        this.keyMap = {
            'enter': playOrPurchaseAsset,
            'cancel stop': stop
        };
    }

    componentDidMount() {
        KeyMapper.bindKeys(this.keyMap);
    }

    componentWillUnmount() {
        KeyMapper.unbindKeys(this.keyMap);
    }

    render() {
        const {
            isModalActive,
            isTrailerPlayout,
            playoutAsset
        } = this.props;

        const isActionButtonVisible = isTrailerPlayout && !isModalActive;

        return (
            <div>
                {isActionButtonVisible && <TrailerActionButton asset={playoutAsset}/>}
            </div>
        );
    }
}

VodTrailerPlayer.propTypes = {
    playoutAsset: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    playout: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    isModalActive: React.PropTypes.bool,
    isTrailerPlayout: React.PropTypes.bool
};

function playoutData(appState) {
    return {
        isModalActive: !!currentModalType(appState),
        isTrailerPlayout: isTrailerPlayout(appState),
        playoutAsset: playoutAsset(appState),
        playout: playout(appState)
    };
}

export default connect(playoutData)(VodTrailerPlayer);
