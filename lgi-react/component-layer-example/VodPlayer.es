import Immutable from 'immutable';
import React from 'react';
import ContainerComponent from 'horizon4-shared/components/container_component';
import Header from 'horizon4-header';
import { connect } from 'react-redux';
import KeyMapper from '../../../key_maps/key_mapper';
import { playoutAsset } from 'domains/playout/vod/selectors';
import {
    header,
    player,
    isTrickPlayerBarShown,
    isTrickPlayerFeedbackShown,
    isNextEpisodeLoading
} from '../selectors';
import { playout } from 'domains/playout/selectors';
import { getTrickPlayerMode } from '../helpers';

import TrickPlayer from '../presentational/trick_player/TrickPlayer';
import TrickFeedback from '../presentational/trick_player/TrickFeedback';
import TrickBar from '../presentational/trick_player/TrickBar';
import TrickBarInfo from '../presentational/trick_player/TrickBarInfo';
import SeekBar from '../presentational/trick_player/SeekBar';

import { msInSecond as SECOND_IN_MS } from '../../../constants/date_constants';

import {
    stop,
    goToMidAssetDetailsPage,
    showUiOrMidAssetDetailsPage,
    jumpForward
} from './PlayerActions';

import {
    toggleTrickPlayer,
    handlePlayPause,
    goToMainMenu,
    handleGoToLiveTvKey,
    fastForward,
    rewind,
    goBack,
    releaseRight,
    releaseLeft,
    jumpBackward
} from '../actions';

class VodPlayer extends ContainerComponent {

    constructor(props) {
        super(props);

        this.keyMap = {
            'menu': goToMainMenu,
            'up': toggleTrickPlayer,
            'down': toggleTrickPlayer,
            'play': handlePlayPause,
            'info': goToMidAssetDetailsPage,
            'space enter': showUiOrMidAssetDetailsPage,
            'stop': stop,
            'cancel': goBack,
            'tv': handleGoToLiveTvKey,
            'fast_fwd': fastForward,
            'rewind': rewind,
            'right:up': releaseRight,
            'left:up': releaseLeft,
            'right:down': jumpForward,
            'left:down': jumpBackward
        };
    }

    componentDidMount() {
        KeyMapper.bindKeys(this.keyMap);
    }

    componentWillUnmount() {
        KeyMapper.unbindKeys(this.keyMap);
    }

    render() {
        const {
            playoutAsset,
            playout,
            isTrickPlayerBarShown,
            isTrickPlayerFeedbackShown,
            header,
            player,
            loadingNextEpisode
        } = this.props;

        // Get the optimistic playRate from the player state.
        const playRate = player.get('playRate');
        const trickPlayMode = getTrickPlayerMode(playRate);

        const position = playout.get('position') * SECOND_IN_MS;
        const duration = playoutAsset.get('durationInSeconds') * SECOND_IN_MS;
        const progress = position / duration;
        const title = playoutAsset.get('title');
        const playoutTimeRemaining = duration - position;
        const showTrickPlayer = (isTrickPlayerBarShown || isTrickPlayerFeedbackShown) && !loadingNextEpisode;

        let trickPlayerComponents = [];
        let headerComponent = null;

        // Only show the trick player + header when we have playout time remaining.
        // There is no use to show a player when there is nothing to play anymore.
        if (playoutTimeRemaining && trickPlayMode) {
            if (isTrickPlayerFeedbackShown) {
                trickPlayerComponents.push(<TrickFeedback key="trickFeedback" playerState={trickPlayMode} />);
            }
            if (isTrickPlayerBarShown) {
                trickPlayerComponents.push(
                    <TrickBar key="trickBar">
                        <TrickBarInfo
                            title={title}
                            timestamp={playoutTimeRemaining} />
                        <SeekBar
                            progress={progress}
                            playerState={trickPlayMode}
                            timestamp={position} />
                    </TrickBar>
                );
            }

            if (showTrickPlayer) {
                headerComponent = (<Header {...header} />);
            }
        }

        return (
            <div>
                {showTrickPlayer && <TrickPlayer>
                    {trickPlayerComponents}
                </TrickPlayer>}
                {headerComponent}
            </div>
        );
    }
}

VodPlayer.propTypes = {
    isTrickPlayerBarShown: React.PropTypes.bool,
    isTrickPlayerFeedbackShown: React.PropTypes.bool,
    playoutAsset: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    playout: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    player: React.PropTypes.instanceOf(Immutable.Map).isRequired,
    header: React.PropTypes.object.isRequired
};

function playerState(appState) {
    return {
        isTrickPlayerBarShown: isTrickPlayerBarShown(appState),
        isTrickPlayerFeedbackShown: isTrickPlayerFeedbackShown(appState),
        playoutAsset: playoutAsset(appState),
        playout: playout(appState),
        header: header(appState),
        player: player(appState),
        loadingNextEpisode: isNextEpisodeLoading(appState)
    };
}

export default connect(playerState)(VodPlayer);
