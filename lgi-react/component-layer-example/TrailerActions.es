// Actions
import { onPlayAsset } from '../../../actions/playout/vod';
import stateStore from '../../../store/state_store';
import { fetchData } from '../../../actions/vod_details_page_actions';
import { currentData } from '../../../selectors/details_page_selectors';

// Selectors
import {
    playoutAsset as currentPlayoutAsset,
    isTrailerPlayout
} from 'domains/playout/vod/selectors';

export function playOrPurchaseAsset() {
    const appState = stateStore.getState();
    const currentAsset = currentPlayoutAsset(appState);

    if (isTrailerPlayout(appState)) {
        // switch from trailer asset to the main asset.
        // Go through the complete watch/purchase flow.
        fetchData(currentAsset.get('crid')).then(() => {
            const asset = currentData(stateStore.getState());
            onPlayAsset(asset);
        });
    }
};
