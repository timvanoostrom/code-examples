import log from 'log';
import { translateFormattedLabel, getNoTranslateLabel } from 'lang';
import { takeEvery, takeLatest } from 'redux-saga';
import { select, put, call } from 'redux-saga/effects';

// Selectors
import { nextEpisode, playoutAsset } from 'domains/playout/vod/selectors';
import { currentData as detailsPageData } from 'selectors/details_page_selectors';
import { pinParental } from 'domains/pin_parental/Selectors';
import { profileAgeRating, profileLanguage } from 'selectors/index';
import { optimisticPlayRate, autoStartNextEpisode, isNextEpisodeLoading } from '../selectors';
import { currentInteractiveToast } from 'selectors/toast_selectors';

// Helpers
import { isVodAssetBlocked } from 'helpers/vod_pin_parental_helpers';

// Actions
import { loadAssetAndGotoVodPlayer } from 'actions/playout/vod';
import { tuneLastChannel } from 'actions/playout/channel';
import { fetchData } from 'actions/vod_details_page_actions';
import { showToast, hideToast } from 'actions/toasts_actions';
import { goToEpisodeDetailsPage, goBack } from 'actions/router_actions';
import { onRestartVodPlayer } from './PlayerActions';
import { hideLoadingModal } from 'actions/playout/loading_modal';

// Action creators
import { cancelNextEpisode, loadingNextEpisode, updateToastMessage } from '../action_creators';

// Constants
import { BINGE_NEXT_EPISODE_STARTS_IN_SECONDS, ActionTypes } from '../constants';
import { ActionTypes as PlayoutDomainActionTypes } from 'domains/playout/constants';
import { PlayoutSuccess, PlayoutErrors } from 'domains/playout/playLoadStopConstants';
import { ToastConstants } from 'horizon4-toast';
import DateConstants from 'constants/date_constants';

const BINGE_WATCH_TOAST_ID = 'bingeWatchingToastForPlayerView';

function hideBingeToast() {
    hideToast(BINGE_WATCH_TOAST_ID);
}

export function stop() {
    hideBingeToast();
    tuneLastChannel();
    goBack();
}

export function* startNextEpisode() {
    // Select the data we pre-fetched when showing the toast for the next episode.
    const nextAsset = yield select(nextEpisode);
    if (nextAsset) {
        hideBingeToast();
        let assetDetails = yield select(detailsPageData);
        if (assetDetails.get('crid') !== nextAsset.get('crid')) {
            yield call(fetchData, nextAsset.get('crid'), true);
            assetDetails = yield select(detailsPageData);
        }
        if (nextAsset.get('crid') === assetDetails.get('crid')) {
            const isEntitled = assetDetails.get('isEntitled');
            const ageAndAdultProps = { ageRating: assetDetails.get('ageRating'), isAdult: assetDetails.get('isAdult') };
            const currentAgeRating = yield select(profileAgeRating);
            const pinParentalState = yield select(pinParental);
            const isBlocked = isVodAssetBlocked(ageAndAdultProps, currentAgeRating, pinParentalState);
            yield put(loadingNextEpisode());
            if (isEntitled && !isBlocked) {
                try {
                    yield call(loadAssetAndGotoVodPlayer, assetDetails);
                } catch (e) {
                    log.error(e);
                    yield call(stop);
                }
            } else {
                try {
                    // Load new details page data with next crid.
                    yield call(goToEpisodeDetailsPage, nextAsset.get('crid'));
                } catch (e) {
                    log.error(e);
                    yield call(stop);
                }
            }
        } else {
            yield call(stop);
        }
    } else {
        yield call(stop);
    }
}

export function* bingeToastMessage(secondsLeft) {
    const episode = yield select(nextEpisode);
    const currentLocale = yield select(profileLanguage);

    const episodeDescription = translateFormattedLabel({
        alias: 'DIC_BINGEVIEWING_TITLE',
        subs: [
            { key: 'seriesTitle', value: episode.get('seriesTitle') },
            { key: 'seasonNumber', value: episode.get('seasonNumber') },
            { key: 'episodeNumber', value: episode.get('episodeNumber') },
            { key: 'episodeTitle', value: episode.get('episodeTitle') }
        ]
    }, currentLocale);

    const episodeTimerText = translateFormattedLabel({
        alias: 'DIC_BINGEVIEWING_TEXT',
        subs: [
            { key: 'count_down', value: secondsLeft }
        ]
    }, currentLocale);

    return getNoTranslateLabel(`${episodeDescription}\n${episodeTimerText}`);
}

export function* showBingeWatchingToast() {
    const message = yield call(bingeToastMessage, BINGE_NEXT_EPISODE_STARTS_IN_SECONDS);
    showToast({
        id: BINGE_WATCH_TOAST_ID,
        message,
        iconType: ToastConstants.IconTypes.PLAY_TYPE,
        focusIndex: 0, // Which button is focused ?
        displayTimeMs: BINGE_NEXT_EPISODE_STARTS_IN_SECONDS * DateConstants.msInSecond,
        addClass: { 'toast--bingeWatching': true },
        buttons: [{
            label: 'DIC_BINGEVIEWING_START',
            action: { type: ActionTypes.START_NEXT_EPISODE }
        },{
            label: 'DIC_BINGEVIEWING_CANCEL',
            action: { type: ActionTypes.CANCEL_NEXT_EPISODE }
        }]
    });
}

export function* handleToastDisplay(action) {
    const autoStart = yield select(autoStartNextEpisode);
    const nextEpisodeLoading = yield select(isNextEpisodeLoading);
    const currentToast = yield select(currentInteractiveToast);
    const isToastActive = currentToast && currentToast.get('id') === BINGE_WATCH_TOAST_ID;
    const currentPlayRate = yield select(optimisticPlayRate);
    const position = action.payload;
    const currentAsset = yield select(playoutAsset);
    const durationInSeconds = currentAsset.get('durationInSeconds');

    if (!isToastActive && autoStart && !nextEpisodeLoading) {
        if (currentPlayRate === '1x' && !isToastActive) {
            const showToastPosition = durationInSeconds - BINGE_NEXT_EPISODE_STARTS_IN_SECONDS;
            if (position >= showToastPosition) {
                yield call(showBingeWatchingToast);
            } else if (isToastActive && position < showToastPosition) {
                hideBingeToast();
            }
        }
    } else if (currentPlayRate === '1x' && isToastActive) {
        const message = yield call(bingeToastMessage, durationInSeconds - position);
        yield put(updateToastMessage({ toastId: BINGE_WATCH_TOAST_ID, message }));
    }
}

function* cancelNextEpisodeAutoStart() {
    yield put(cancelNextEpisode());
    yield call(hideBingeToast);
}

export function* playoutEnd(action) {
    const status = action.payload;
    // Only respond to the end when we automatically ended the playout.
    const autoStart = yield select(autoStartNextEpisode);
    if (status.name === PlayoutSuccess.SUCCESS_PLAYOUT_AUTOMATIC_END_OF_CONTENT_REACHED && autoStart) {
        yield call(startNextEpisode);
    } else if (status.name !== PlayoutErrors.ERROR_NEW_PLAYOUT_REQUESTED) {
        yield call(stop);
    }
}

function* simulatePlayerRouteEntry() {
    yield call(onRestartVodPlayer);
    hideLoadingModal();
}

export function takeEveryPlayoutEnd() {
    return call(takeEvery, PlayoutDomainActionTypes.PLAYOUT_END, playoutEnd);
}

export function getWatchers() {
    return [
        call(takeLatest, ActionTypes.START_NEXT_EPISODE, startNextEpisode),
        call(takeLatest, ActionTypes.CANCEL_NEXT_EPISODE, cancelNextEpisodeAutoStart),
        call(takeEvery, PlayoutDomainActionTypes.UPDATE_PLAYOUT_POSITION, handleToastDisplay),
        call(takeEvery, ActionTypes.RENEW_PLAYER, simulatePlayerRouteEntry),
        takeEveryPlayoutEnd()
    ];
}

export default function* bingeWatchingSaga() {
    const autoStart = yield select(autoStartNextEpisode);
    if (autoStart) {
        yield getWatchers();
    } else {
        yield takeEveryPlayoutEnd();
    }
}
