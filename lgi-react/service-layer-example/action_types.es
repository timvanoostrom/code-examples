import nsKeyMirror from 'nskeymirror';

export default nsKeyMirror({
    INIT_SETTINGS: null,
    UPDATE_SETTING: null,
    HANDLE_CHANGE_SETTING: null
}, 'settingsActionTypes');
