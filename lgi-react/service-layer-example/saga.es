import _ from 'lodash';
import log from 'log';
import { eventChannel, takeEvery } from 'redux-saga';
import { call, put, select, spawn, take, cancelled } from 'redux-saga/effects';
import ActionTypes from './action_types';
import * as Actions from './action_creators';
import * as Service from './service';
import { settings as allSettings } from './selectors';
import { ServiceMap } from './service';

export default function* settingsDomainSaga() {
    const observers = yield spawn(initializeObservers);
    yield call(initializeSettings);
    return observers;
}

function* initializeSettings() {
    try {
        const settings = yield call(Service.getSettings);
        yield put(Actions.initSettings(settings));
        yield call(Service.updateProfileVersion);
        log.debug('Settings Domain is Initialized');
    } catch (error) {
        log.error(`Settings Domain Initialization Error: ${error}`);
    }
}

function* initializeObservers() {
    yield take(ActionTypes.INIT_SETTINGS);
    yield [
        currentProfileStatusObserver(),
        changeSettingObserver()
    ];
}

function* handleChangeAction(action) {
    const { payload } = action;
    const { id, value } = payload;
    const settings = yield select(allSettings);
    const currentValue = settings.get(id);

    if (_.isUndefined(currentValue)) {
        log.error(`handleChangeAction(action) saga requires a valid settingsDomain ID in the payload. Passed id: ${id}`);
        return;
    }

    // Just return when the value didn't change.
    if (value === currentValue) {
        return;
    }

    const serviceCall = ServiceMap[id];
    let newValue;
    try {
        newValue = serviceCall ? yield call(serviceCall, value) : value;
    } catch (e) {
        log.error(`The service did not respond upon change value request for ${id}`);
        return;
    }

    yield put(Actions.updateSettingInReducer({ [id]: newValue }));
}

function* changeSettingObserver() {
    yield* takeEvery(ActionTypes.HANDLE_CHANGE_SETTING, handleChangeAction);
}

/* Event channel observers */
function currentProfileStatusChannel() {
    return eventChannel(emitter => {
        return Service.createCurrentProfileStatusObserver(code => emitter(code));
    });
}

function* currentProfileStatusObserver() {
    const channel = yield call(currentProfileStatusChannel);
    try {
        while (true) {
            const payload = yield take(channel);
            yield put(Actions.updateSettingInReducer(payload));
        }
    } catch (e) {
        log.error(e);
        if (yield cancelled()) {
            channel.close();
        }
    }
}
