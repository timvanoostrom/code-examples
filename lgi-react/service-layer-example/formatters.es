import _ from 'lodash';
import { SettingIds, DefaultValues, AudioTypes, DEFAULT_COUNTRY } from './constants';

/**
 * Returns formatted NAF model.state.settings
 *
 * @param {NAF.Model.Settings} nafSystem
 * @return {Object} formatted settings props
 */
export function formatNAFSettings(nafSettings = {}) {
    const profile = _.get(nafSettings, 'current.profile', {});
    const output = _.get(nafSettings, 'output', {});
    const ageRating = profile.ageRating && profile.ageRating;
    return {
        [SettingIds.AGE_RATING]: ageRating,
        [SettingIds.AUDIO_FORMAT]: _.get(output, 'audio.format', null),
        [SettingIds.AUDIO_TYPE]: _.get(profile, 'prefAudioLangs.0.type') || AudioTypes.NORMAL,
        [SettingIds.AUDIO_LANGUAGE]: _.get(profile, 'prefAudioLangs.0.code', null),
        [SettingIds.COUNTRY]: _.get(profile, 'country', DEFAULT_COUNTRY),
        [SettingIds.UI_LANGUAGE]: _.get(profile, 'view.lang.code', null),
        [SettingIds.PERSONALISATION_OPT_IN]: _.get(profile, 'optIn', DefaultValues.OFF),
        [SettingIds.SUBTITLES_TYPE]: _.get(profile, 'prefSubtitleLangs.0.active') ? _.get(profile, 'prefSubtitleLangs.0.type') : DefaultValues.OFF,
        [SettingIds.SUBTITLES_LANGUAGE]: _.get(profile, 'prefSubtitleLangs.0.code', null),
        [SettingIds.RECORD_PADDING_FRONT]: _.get(nafSettings, 'pvr.frontPad', null),
        [SettingIds.RECORD_PADDING_BACK]: _.get(nafSettings, 'pvr.backPad', null),
        [SettingIds.STANDBY_TIMEOUT]: _.get(nafSettings, 'autoStandby.inactivityTimeout', null),
        [SettingIds.STANDBY_MODE]: _.get(nafSettings, 'power.defaultStandby', null),
        [SettingIds.REPLAY_OPT_IN_START_DATE]: _.result(profile.tstvOptInDate, 'getTime', DefaultValues.OFF),
        [SettingIds.PRIMARY_VIDEO_OUTPUT]: output.primary === 'auto' ? 'hdmi' : output.primary,
        [SettingIds.HDMI_RESOLUTION]: _.get(output, 'hdmi.resolution', null),
        [SettingIds.HDMI_FORMAT]: _.get(output, 'hdmi.format', null),
        [SettingIds.TV_ASPECT_RATIO]: _.get(output, 'tv.aspectRatio', null),
        [SettingIds.TV_START_TYPE]: _.get(output, 'tv.start', null),
        [SettingIds.IMAGE_SIZING_4_3]: _.get(output, 'scart.format', null),
        [SettingIds.IMAGE_SIZING_16_9]: _.get(output, 'scart.format', null),
        [SettingIds.PROFILE_ID]: _.get(profile, 'id', null),
        [SettingIds.PROFILE_STATUS]: _.get(profile, 'status.code', null)
    };
}
