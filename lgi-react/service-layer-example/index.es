import * as settingsActions from './actions';
import * as settingsSelectors from './selectors';
import * as settingsConstants from './constants';
import * as settingsHelpers from './helpers';
import Saga from './saga';

export default {
    Actions: settingsActions,
    Selectors: settingsSelectors,
    Constants: settingsConstants,
    Helpers: settingsHelpers,
    Saga: Saga
};

