import Immutable from 'immutable';
import ActionTypes from './action_types';;
import { formatNAFSettings } from './formatters';

const initialState = Immutable.fromJS(formatNAFSettings());

export default function settingsDomainReducer(state = initialState, action = {}) {
    const { payload } = action;
    switch (action.type) {
        case ActionTypes.INIT_SETTINGS:
            return state.mergeDeep(action.payload);

        // General action for setting id => value pairs
        case ActionTypes.UPDATE_SETTING:
            return state.merge(payload);

        default:
            return state;
    }
}
