import _ from 'lodash';
import { createSelector } from 'reselect';
import conf from 'conf';

/**
 * Selects system mode from global state object
 * @param  {Immutable} state Global state
 * @return {constants.SystemModes} The current system mode
 */

// import { settingSelector } from './helpers';
import { SettingIds, DEFAULT_LOCALE } from './constants';

export function settings(state) {
    return state.get('settings');
}

export function settingSelector(state, id) {
    return state.getIn(['settings', id]);
}

export const setting = settingSelector;

export const currentLocale = createSelector([language, country], (language, country) => {
    const availableLangs = conf.get('available-menu-languages');
    const countryLanguageMappings = conf.get('country-language-mappings');
    const localeConfig = _(countryLanguageMappings[country])
        .map((i18nKey) => availableLangs[i18nKey])
        .find((confLang) => confLang.iso6392 === language || confLang.iso6391 === language);
    return _.get(localeConfig, 'i18n', DEFAULT_LOCALE);
});

export function currentAgeRating(state) {
    return settingSelector(state, SettingIds.AGE_RATING);
}

export function language(state) {
    return settingSelector(state, SettingIds.UI_LANGUAGE);
}

export function currentProfileId(state) {
    return settingSelector(state, SettingIds.PROFILE_ID);
}

export function country(state) {
    return settingSelector(state, SettingIds.COUNTRY);
}

export function personalisationOptIn(state) {
    return settingSelector(state, SettingIds.PERSONALISATION_OPT_IN);
}

export function replayOptInStartTimeStamp(state) {
    return settingSelector(state, SettingIds.REPLAY_OPT_IN_START_DATE);
}

export function audioFormat(state) {
    return settingSelector(state, SettingIds.AUDIO_FORMAT);
}
