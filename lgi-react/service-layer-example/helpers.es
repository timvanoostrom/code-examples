import DateConstants from 'constants/date_constants';

/**
 * Checks if Replay Opt-In is activated
 * @param  {Number|null} tstvOptInTimeStamp optIn activation time stamp
 * @param  {Int} nowTimeStamp current time timestamp (Date.now())
 * @return {Boolean} True if Opt In is activated
 */
export function isReplayOptInActivated(tstvOptInTimeStamp, nowTimeStamp) {
    //as soon as applicationTime is updated once per minute but tstvOptInDate has seconds precision
    //we hasve to add 1 minute to the current application time
    return tstvOptInTimeStamp && tstvOptInTimeStamp < (nowTimeStamp + DateConstants.msInMinute);
};
