import conf from 'conf';
import keyMirror from 'nskeymirror';

// Available values for model.state.settings.current.profile.prefAudioLangs.0.type
export const AudioTypes = {
    AUDIO_DESCRIPTION: 'AD',
    NORMAL: 'NR',
};

// Map of system modes (model.state.settings.output.audio)
export const AudioFormats = {
    AUTO: 'auto',
    STEREO: 'stereo',
    MONO: 'mono',
    DOLBY: 'dolby'
};

// Available values for model.state.settings.output.tv.start
export const TvStartType = {
    AUTO: 'auto',
    MANUAL: 'manual',
};

export const VERSION = '1';

export const SettingIds = keyMirror({
    // A user is not able to changes these settings via the UI.
    PROFILE_STATUS: null,
    PROFILE_ID: null,
    COUNTRY: null,

    // Settings that can be changed by the user via the UI.
    UI_LANGUAGE: null,
    AGE_RATING: null,
    AUDIO_TYPE: null,
    AUDIO_LANGUAGE: null,
    PERSONALISATION_OPT_IN: null,
    TV_START_TYPE: null,
    SUBTITLES_TYPE: null,
    SUBTITLES_LANGUAGE: null,
    RECORD_PADDING_FRONT: null,
    RECORD_PADDING_BACK: null,
    STANDBY_TIMEOUT: null,
    STANDBY_MODE: null,
    REPLAY_OPT_IN_START_DATE: null,
    AUDIO_FORMAT: null,
    PRIMARY_VIDEO_OUTPUT: null,
    HDMI_RESOLUTION: null,
    HDMI_FORMAT: null,
    TV_ASPECT_RATIO: null,
    IMAGE_SIZING_4_3: null,
    IMAGE_SIZING_16_9: null
});

export const DefaultValues = {
    OFF: false,
    ON: true
};

const defaultMenuSetting = conf.get('menu-language');

export const DEFAULT_LOCALE = defaultMenuSetting.i18n;
export const DEFAULT_LANGUAGE = defaultMenuSetting.isoCode;
export const DEFAULT_COUNTRY = DEFAULT_LOCALE.split('-').pop().toUpperCase();

export const SETTINGS_TO_RESET = [
    'model.state.settings.pvr.seriesDomain',
    'model.state.settings.pvr.diskManagement',
    'model.state.settings.profiles.0.needPinForPurchase',
    'model.state.settings.output.scart.enable',
    'model.state.settings.output.hdmi.audio.enable',
    'model.state.settings.output.hdmi.audio.delay',
    'model.state.settings.output.spdif.enable',
    'model.state.settings.output.spdif.delay',
    'model.state.settings.output.hdmi.color',
    'model.state.settings.autoStandby.inactivityTimeout'
];
