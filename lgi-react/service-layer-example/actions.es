import _ from 'lodash';
import conf from 'conf';
import stateStore from 'store/state_store';

import * as Service from './service';
import * as Actions from './action_creators';

import { settingSelector } from './selectors';
import { SettingIds } from './constants';

// TODO: Remove when https://github.com/LibertyGlobal/Horizon4/issues/3360 is implemented.
(function addUpperCasedIsoCodes() {
    function ucIsoCodesArray(row) {
        Object.assign(row, { isoCodes: row.isoCodes.concat(row.isoCodes.map((code) => code.toUpperCase())) });
    }
    _.forEach(conf.get('preferred-audio-languages'), ucIsoCodesArray);
    _.forEach(conf.get('subtitles-languages'), ucIsoCodesArray);
    _.forEach(conf.get('known-languages'), ucIsoCodesArray);
    _.forEach(conf.get('special-languages'), ucIsoCodesArray);
})();

/* Actions used by First Install flow */
export function updateUILanguage(language) {
    const state = stateStore.getState();
    const currentLanguage = settingSelector(state, SettingIds.UI_LANGUAGE);
    if (currentLanguage !== language) {
        return Service.setUILanguage(language).then((lang) => {
            stateStore.dispatch(Actions.updateSettingInReducer({
                [SettingIds.UI_LANGUAGE]: lang
            }));
        });
    }
    return Promise.resolve();
}

export function updatePersonalisation(value) {
    const state = stateStore.getState();
    const personalisationOptIn = settingSelector(state, SettingIds.PERSONALISATION_OPT_IN);
    if (personalisationOptIn !== value) {
        return Service.setPersonalisation(value).then((optIn) => {
            stateStore.dispatch(Actions.updateSettingInReducer({
                [SettingIds.PERSONALISATION_OPT_IN]: optIn
            }));
        });
    }
    return Promise.resolve();
}
