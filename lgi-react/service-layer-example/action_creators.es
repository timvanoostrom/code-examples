import ActionTypes from './action_types';

export function initSettings(settings) {
    return {
        type: ActionTypes.INIT_SETTINGS,
        payload: settings,
    };
}

export function changeSetting(payload) {
    return {
        type: ActionTypes.HANDLE_CHANGE_SETTING,
        payload
    };
}

export function updateSettingInReducer(payload) {
    return {
        type: ActionTypes.UPDATE_SETTING,
        payload
    };
}
