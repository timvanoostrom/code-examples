<template>
  <div class="node-search">
    <div class="node-search-input-section">
      <icon-btn v-if="formData.search !== ''" icon="remove" class="search-utility-icon" @click="clearSearch" />
      <icon v-else name="search" class="search-utility-icon" />
      <input
        ref="inputText"
        type="text"
        :value="formData.search"
        @keyup="handleInputKeyUp"
        @blur="hideResultsBlur"
        @keyup.esc="hideResultsEsc"
        @keydown.up.prevent="selectResultAbove"
        @keydown.down.prevent="selectResultDown"
        @keyup.enter.prevent="selectResultActive"
        class="node-search-input"
        :placeholder="$t(placeholder)" />
    </div>
    <ul :hidden="!resultsVisible" ref="resultsList" class="node-search-results" :class="[resultsPosition]" :style="`max-height: ${resultsHeight}px;`">
      <li v-for="(node, index) of searchResults" :key="node.id" @click.stop="handleClick(node)" class="node-search-results-item" :class="{ 'is-highlighted': isHighlighted(index), 'is-included': isSelected(node.id) }">
        <span class="node-search-results-title" v-html="node.matchedTitle"></span>
      </li>
      <li class="node-search-results-item node-search-results--no-results">{{ $t('No search results') }}</li>
    </ul>
  </div>
</template>

<script>
import { mapGetters } from 'vuex';
import { elementPosition } from 'helpers/dom';
import { nodeTypes } from './constants';
import _throttle from 'lodash.throttle';
import _debounce from 'lodash.debounce';

let handleSearchInput;
let handleToggleSearch;

const THROTTLE_INPUT_MS = 400;
const TOGGLE_MS = 400;
const RESULTS_LIMIT = 400;

export default {
  name: 'NodeSearch',

  props: {
    max: {
      type: Number,
      default: null
    },

    multi: {
      type: Boolean,
      default: false
    },

    selected: {
      type: Array,
      default: () => ([])
    },

    restrictedIds: {
      type: Array,
      default: () => ([])
    },

    restrictedTypes: {
      type: Array,
      default: () => ([nodeTypes.LEDGER, nodeTypes.LEDGER_TOTAL, nodeTypes.LEDGER_TOTAL_VARIABLE])
    },

    persistSearchTerm: {
      type: Boolean,
      default: false
    },

    resultsPosition: {
      type: String,
      default: 'match-width'
    },

    excludeSelected: {
      type: Boolean,
      default: true
    },

    placeholder: {
      type: String,
      default: 'Search'
    }
  },

  data() {
    return {
      nodesSelected: [ ...this.selected ],
      formData: {
        search: ''
      },
      resultsVisible: false,
      resultsHeight: 0,
      highlightedIndex: null
    };
  },

  created() {
    if (this.persistSearchTerm) {
      // Pre-fill the selection
      this.formData.search = this.selected.length === 1 ? this.pathTitle(this.selected[0]) : '';
    }

    handleSearchInput = _throttle((handleInput) => {
      handleInput();
    }, THROTTLE_INPUT_MS);

    handleToggleSearch = _debounce((toggle) => {
      toggle();
    }, TOGGLE_MS);
  },

  watch: {
    'formData.search'(a, b) {
      if (!this.multi) {
        const [selected] = this.nodesSelected;
        if (this.nodes[selected] && b === this.nodes[selected].title) {
          this.nodesSelected.splice(0, 1);
          this.$emit('select', { selected: this.nodesSelected });
        }
      }
      const termExtends = a.startsWith(b);
      if (!termExtends || (termExtends && b.length < a.length)) {
        this.matchedNodes = null;
        this.highlightedIndex = null;
      }
    },

    selected(selected) {
      this.nodesSelected = [ ...selected ];
    }
  },

  computed: {
    ...mapGetters('driverTree', ['nodes', 'searchNodes']),

    allowedSearchNodes() {
      return this.searchNodes.filter(({ id, blockType, title, ledger }) => {
        return !this.restrictedIds.includes(id) && !this.restrictedTypes.includes(blockType) && !(this.excludeSelected && this.nodesSelected.includes(id));
      }).map(({ id, title }) => {
        return {
          id,
          title: this.pathTitle(id)
        };
      });
    },

    searchResults() {
      return this.formData.search ? this.allowedSearchNodes.map(({ id, title }) => {
        return {
          matches: this.isMatch(title),
          title,
          id
        };
      }).filter(({ matches }) => !!matches).map(({ id, title, matches }) => {
        return {
          matchedTitle: this.matchTitle(title, matches),
          title,
          id
        };
      }).slice(0, RESULTS_LIMIT) : [];
    },
  },

  methods: {
    isMatch(title) {
      const segments = this.formData.search.trim().split(' ');
      const matches = segments.map(segment => {
        const matches = title.match(new RegExp(segment, 'gi'));
        if (matches) {
          return matches[0];
        }
      }).filter(match => !!match);

      return matches.length === segments.length ? matches : null;
    },

    matchTitle(title, matches) {
      if (matches) {
        return matches.reduce((title, term) => {
          return title.replace(term, `<b>${term}</b>`);
        }, title);
      }
      return title;
    },

    handleClick(node) {
      if (this.nodesSelected.includes(node.id) && this.multi) {
        this.deselectNode(node.id);
      } else {
        this.selectNode(node.id);
      }
    },

    selectNode(id) {
      if (this.multi && this.max && this.nodesSelected.length === this.max) {
        this.nodesSelected.splice(0, 1);
        this.nodesSelected.push(id);
      } else {
        this.nodesSelected.push(id);
      }

      if (this.persistSearchTerm) {
        this.formData.search = this.pathTitle(id);
      } else {
        this.formData.search = '';
      }

      this.hideResults();

      this.$emit('select', { selected: this.nodesSelected, id });
    },

    deselectNode(id) {
      this.nodesSelected = this.nodesSelected.filter(eId => eId !== id);
      this.$emit('select', { selected: this.nodesSelected });
    },

    handleInputKeyUp(event) {
      if (event.key !== 'Escape' && event.key !== 'Enter') {
        handleSearchInput(() => {
          if (event.target.value !== '') {
            this.formData.search = event.target.value;
            if (!this.resultsVisible) {
              this.showResults();
            }
          } else {
            this.hideResults();
          }
        });
      }
    },

    showResults() {
      if (this.formData.search.length >= 2) {
        this.resultsVisible = true;
        this.$refs.inputText.focus();
        this.$nextTick(() => {
          this.resultsHeight = window.innerHeight - elementPosition(this.$refs.resultsList).y;
        });
      }
    },

    hideResultsEsc(event) {
      if (this.resultsVisible) {
        event.stopPropagation();
        this.hideResults();
      }
    },

    hideResultsBlur(event) {
      if (event.key !== 'Tab' && !this.$el.contains(event.target)) {
        this.hideResults();
      }
    },

    hideResults() {
      // Hide the results with a timeout so the dom doesn't think we are clicking (mouseup)
      // on items on the lower levels of the DOM.
      this.resultsVisible = false;
      this.highlightedIndex = null;
    },

    clearSearch() {
      this.formData.search = '';
      this.highlightedIndex = null;
      this.$nextTick(() => {
        this.$refs.inputText.focus();
      });
    },

    toggleSearchDebounced() {
      handleToggleSearch(() => {
        if (this.resultsVisible) {
          this.hideResults();
        } else {
          this.showResults();
        }
      });
    },

    selectResultAbove() {
      if (this.highlightedIndex === 0) {
        this.highlightedIndex = null;
      } else if (this.highlightedIndex !== null) {
        this.highlightedIndex -= 1;
      }
    },

    selectResultDown() {
      if (this.highlightedIndex === (this.searchResults.length - 1)) {
        this.highlightedIndex = null;
      } else if (this.highlightedIndex !== null) {
        this.highlightedIndex += 1;
      } else {
        this.highlightedIndex = 0;
      }
    },

    isHighlighted(index) {
      return this.highlightedIndex === index;
    },

    isSelected(id) {
      return this.selected.includes(id);
    },

    selectResultActive() {
      if (this.highlightedIndex !== null) {
        this.selectNode(this.searchResults[this.highlightedIndex].id);
      }
    },

    pathTitle(id) {
      const node = this.nodes[id];
      return node.allParentIds.map(id => this.nodes[id].title).concat(node.title).join(' > ');
    }
  }
};
</script>

<style lang="scss">
.node-search {
  width: 100%;
  position: relative;

  &-input-section {
    position: relative;
    border-bottom: 1px solid $grey200;
  }

  &-suggestion {
    display: block;
    position: absolute;
    z-index: 1;
    background: red;
  }

  &-input {
    border: 0;
    outline: 0;
    padding: 3px 13px;
    background: transparent;
    color: $grey600;
    z-index: 2;
    position: relative;
    width: 100%;

    &::placeholder {
      color: $grey400;
    }
  }

  .search-utility-icon {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 0px;
    color: $grey400;
    z-index: 3;
    cursor: pointer;
    &:hover {
      color: $grey800;
    }
  }

  &-results {
    top: 21px;
    min-width: 400px;
    position: absolute;
    background-color: $white;
    border: 1px solid $grey100;
    border-top: 0;
    box-shadow: 2px 2px 12px rgba(155, 155, 155, .2);
    max-height: 400px;
    overflow: hidden;
    overflow-y: scroll;
    z-index: z($z_indices, search-component-results);
    list-style-type: none;
    padding: 0;
    border: 2px solid $grey200;

    &.center {
      left: 50%;
      transform: translateX(-50%);
    }
    &.left {
      left: 0;
    }
    &.right {
      right: 0;
    }
    &.match-width {
      left: 0;
      width: auto;
    }

    &-item {
      font-size: 12px;
      height: 28px;
      line-height: 28px;
      padding: 0 10px;
      letter-spacing: 0.05em;
      white-space: nowrap;
      cursor: pointer;

      &--no-results {
        background-color: $pwc200;
      }

      &:nth-child(odd) {
        background-color: $grey50;
      }

      &:hover {
        background-color: $grey300;
      }

      &:not([hidden]) + .node-search-results--no-results {
        display: none;
      }

      &.is-highlighted {
        background-color: $pwc100;
      }
    }

    &-title {
      display: inline-block;
      vertical-align: middle;
    }
  }
}

.node-search .node-path {
  display: inline-block;
  vertical-align: middle;
  margin-left: 10px;
  width: auto;

  .node-path-segment {
    font-size: 11px;
    color: $pwc200;
  }
}
</style>
